<?php

$config = [
    'title' => 'Contact Me',
    'intro' => 'Complete the form below to send me an email.',
    'colors' => [
        'background' => '#1565C0',
        'button' => '#1565C0',
        'buttonHover' => '#0D47A1',
    ],
    'name' => 'John Doe',
    'email' => 'john.doe@example.com',
    'fromname' => 'Example.com',
    'fromemail' => 'support@example.com',
    'recaptchaSiteKey' => '[recaptcha-site-key]',
    'recaptchaSecretKey' => '[recaptcha-secret-key]',
    'smtp' => [
        'host' => '[smtp-host]',
        'port' => '[smtp-port]',
        'security' => '[ssl-or-tls]',
        'username' => '[smtp-username]',
        'password' => '[smtp-password]',
    ],
    'fields' => [
        'name' => [
            'label' => 'Name',
            'type' => 'text',
            'required' => true,
            'placeholder' => 'John Doe',
            'autofocus' => true,
        ],
        'email' => [
            'label' => 'Email Address',
            'type' => 'email',
            'required' => true,
            'placeholder' => 'john@example.com',
        ],
        'subject' => [
            'label' => 'Subject',
            'type' => 'text',
            'required' => true,
            'placeholder' => 'Just saying hi',
        ],
        'message' => [
            'label' => 'Message',
            'type' => 'textarea',
            'required' => true,
            'placeholder' => 'Type your message here',
        ],
        'captcha' => [
            'label' => 'Help keep my inbox spam free',
            'type' => 'captcha',
        ],
    ]
];

return $config;
