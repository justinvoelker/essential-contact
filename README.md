# Essential Contact (Archived)

This was meant to be a quick project for a simple, single page site acting
as a placeholder for a domain being used solely for email. Since it is a
little messy and really just a hodgepodge of manual inclusions, perhaps it
should be rewritten using something like Symfony as a simple application.

Symfony may be overkill, but, since this project will have essentially
nothing to it, it should end up being pretty small.

# Essential Contact

A simple contact form to be used as a single page website.

## Docker compose example

```
version: '3'
services:
  contact:
    image: registry.gitlab.com/justinvoelker/essential-contact
    volumes:
      - ./config.php:/var/www/html/config/web.php
```

Simply use the `config/web-sample.php` file as a starting point for your own
configuration file. Mount your customized config file as
`/var/www/html/config/web.php` and start the container. The contact form is
available on port 80 (or you may use a reverse proxy of your choosing).
