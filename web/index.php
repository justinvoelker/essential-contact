<?php
require __DIR__ . '/../vendor/autoload.php';
$config = require(__DIR__ . '/../config/web.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?= $config['title'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="css/styles.min.css" rel="stylesheet">

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <style>
        body { background-color: <?= $config['colors']['background'] ?> }
        #contact h3 { color: <?= $config['colors']['background'] ?> }
        #contact button[type="submit"] { background-color: <?= $config['colors']['button'] ?> }
        #contact button[type="submit"]:hover { background-color: <?= $config['colors']['buttonHover'] ?> }
    </style>
</head>
<body>

<div class="container">

    <?php
    // This is the current page without the trailing filename
    $pageSelf = htmlentities(substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/') + 1));

    // If post, verify captcha (if present) and send email
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $errors = null;
        if (array_key_exists('captcha', $config['fields'])) {
            $recaptcha = new \ReCaptcha\ReCaptcha($config['recaptchaSecretKey']);
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if ($resp->isSuccess()) {
                // verified!
            } else {
                $errors = $resp->getErrorCodes();
            }
        }
        echo '<div id="contact">';
        if ($errors) {
            echo '<h3>Error</h3>';
            echo '<p>Something was wrong with your captcha response and you will need to try again.</p>';
        } else {
            // Gather the submitted data (but remove the captcha response)
            $data = array();
            foreach ($config['fields'] as $id => $field) {
                if ($field['type'] != 'captcha') {
                    $data[$id] = $_POST[$id];
                }
            }

            // Build email body that contains all of the collected data
            $body = '';
            foreach ($data as $id => $value) {
                $body .= $config['fields'][$id]['label'] . ': ' . $value . "<br>";
            }

            // A little extra that will be appended to the email but not displayed on the submitted for page
            $bodyAddl .= '<br><span style="color: #aaa; font-size: smaller;">';
            $bodyAddl .= 'sent via ' . $_SERVER['SERVER_NAME'] . $pageSelf;
            $bodyAddl .= '</span>';

            /**
             * Build and send the email
             *
             * @var \Swift_Mime_Message $message
             */
            $transport = Swift_SmtpTransport::newInstance(
                $config['smtp']['host'],
                $config['smtp']['port'],
                $config['smtp']['security'])
                ->setUsername($config['smtp']['username'])
                ->setPassword($config['smtp']['password']);
            $mailer = Swift_Mailer::newInstance($transport);
            $message = Swift_Message::newInstance('swiftmailer')
                ->setFrom([$config['fromemail'] => $config['fromname']])
                ->setReplyTo([$data['email'] => $data['name']])
                ->setTo([$config['email'] => $config['name']])
                ->setSubject('Contact Form at ' . $_SERVER['SERVER_NAME'])
                ->setBody($body . $bodyAddl, 'text/html');
            $result = $mailer->send($message);

            echo '<h3>Sent</h3>';
            echo '<p>The following email has been sent. Click <a href="' . $pageSelf . '">here</a> to send another.</p>';
            echo '<p>' . $body . '</p>';
        }
        echo '</div>';
    } else {
        ?>
        <form id="contact" action="<?= $pageSelf ?>" method="post">
            <h3><?= $config['title'] ?></h3>
            <p><?= $config['intro'] ?></p>
            <?php foreach ($config['fields'] as $id => $field): ?>
                <?= field($id, $field, $config) ?>
            <?php endforeach; ?>
            <fieldset>
                <button name="submit" type="submit" id="contact-submit" data-submit="Sending">Submit</button>
            </fieldset>
        </form>
        <?php
    }
    ?>
</div>

</body>
</html>

<?php
function field($id, $field, $config)
{
    if ($field['type'] == 'captcha') {

        $result = '<fieldset class="contains-captcha">' . "\n";
        $result .= '<label>' . $field['label'] . '</label>' . "\n";
        $result .= '<div class="g-recaptcha" data-sitekey="' . $config['recaptchaSiteKey'] . '"></div>' . "\n";
        $result .= '</fieldset>' . "\n";

    } else {

        $pieces = array();

        if ($field['type'] == 'textarea') {
            $pieces[] = 'textarea';
        } else {
            $pieces[] = 'input';
            $pieces[] = 'type="' . $field['type'] . '"';
        }
        $pieces[] = 'id="' . $id . '"';
        $pieces[] = 'name="' . $id . '"';
        $pieces[] = (array_key_exists('placeholder', $field) && $field['placeholder']) ? 'placeholder="' . $field['placeholder'] . '"' : null;
        $pieces[] = (array_key_exists('required', $field) && $field['required']) ? 'required' : null;
        $pieces[] = (array_key_exists('autofocus', $field) && $field['autofocus']) ? 'autofocus' : null;

        $result = '<fieldset>' . "\n";
        $result .= '<label>' . $field['label'] . '</label>' . "\n";
        $result .= '<' . implode(' ', array_filter($pieces)) . '>' . "\n";
        if ($field['type'] == 'textarea') {
            $result .= '</textarea>';
        }
        $result .= '</fieldset>' . "\n";

    }

    return $result;
}

?>
