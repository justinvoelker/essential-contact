FROM php:7.1-fpm-alpine

# Copy project into image
COPY . /var/www/html/

RUN set -xe \
    # Install packages
    && apk add --no-cache \
       nginx \
       supervisor \
    # PHP configugration and log forwarding
    && mkdir -p /var/log/php \
    && echo "expose_php = Off" >> /usr/local/etc/php/php.ini \
    && echo "max_execution_time = 300" >> /usr/local/etc/php/php.ini \
    && echo "log_errors = On" >> /usr/local/etc/php/php.ini \
    && echo "error_log = /var/log/php/error.log" >> /usr/local/etc/php/php.ini \
    && ln -sf /dev/stderr /var/log/php/error.log \
    # Nginx configuration and log forwarding
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    # Install composer, install packages, remove composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && composer install -d /var/www/html --no-dev \
    && rm /usr/bin/composer && rm -r /root/.composer \
    && chown root:www-data -R /var/www/html/* \
    && chmod -R g-w /var/www/html/*

# Copy files into image
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/supervisord.conf /etc/supervisord.conf

# Expose port 80 for nginx
EXPOSE 80

# Run supervisor
CMD ["/usr/bin/supervisord"]
